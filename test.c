#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "usf.h"
#include "usf_internal.h"

int SampleRate = 32000, fd = 0, firstWrite = 1, curHeader = 0;
int bufptr=0;
int AudioFirst = 0;
int first = 1;
#define BUGSIZE 32768


#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/soundcard.h>


void OpenSound()
{
  	fd = open("/dev/dsp", O_RDWR);
	if (fd < 0) {
		perror("open of /dev/dsp failed");
		exit(1);
	}
}


void AddBuffer2(usf_state_t * state, unsigned char *buf, unsigned int length)
{

	int arg = 0, status = 0;

	if(!AudioFirst) {
		AudioFirst = 1;

		SampleRate = 48681812 / (AI_DACRATE_REG + 1);

		OpenSound();
	}

	arg = 16;	   /* sample size */
	status = ioctl(fd, SOUND_PCM_WRITE_BITS, &arg);
	if (status == -1)
    	perror("SOUND_PCM_WRITE_BITS ioctl failed");
  	if (arg != 16)
		perror("unable to set sample size");

	arg = 2;  /* mono or stereo */
	status = ioctl(fd, SOUND_PCM_WRITE_CHANNELS, &arg);
	if (status == -1)
		perror("SOUND_PCM_WRITE_CHANNELS ioctl failed");
	if (arg != 2)
		perror("unable to set number of channels");

	arg = SampleRate;	   /* sampling rate */
	status = ioctl(fd, SOUND_PCM_WRITE_RATE, &arg);
	if (status == -1)
		perror("SOUND_PCM_WRITE_WRITE ioctl failed");

   status = write(fd, buf, length); /* play it back */
    if (status != length)
      perror("wrote wrong number of bytes");

}




unsigned int buffersize = 0;
unsigned char buffer[BUGSIZE];

#include <stdio.h>

double play_time = 0;

void PlayBuffer(usf_state_t * state, unsigned char *buf, unsigned int length) {
	int32_t i = 0;
	int32_t rel_volume = 1.0;
	if(!AudioFirst) {
		AudioFirst = 1;
		OpenSound();
	}

	for(i = 0; i < (length >> 1); i+=2) {
		int32_t r = ((short*)buf)[i];
		int32_t l = ((short*)buf)[i + 1];

		((short*)buffer)[(buffersize>>1) + i + 1] = r;
		((short*)buffer)[(buffersize>>1) + i] = l;
	}

	buffersize+=length;

	if(buffersize > (32768-length)) {
		//writeAudio(hWaveOut, buffer, buffersize);
		AddBuffer2(state, buffer, buffersize);
		buffersize = 0;
	}

}


int usf_test_load_from_file(usf_state_t * state, const char * fn)
{
	FILE * fil = NULL;
	uint32_t reservedsize = 0, codesize = 0, crc = 0, tagstart = 0, reservestart = 0;
	uint32_t i, filesize = 0, tagsize = 0, temp = 0;
	uint8_t buffer[16], * buffer2 = NULL, * tagbuffer = NULL;
	uint32_t enablecompare = 0, enableFIFOfull = 0, rom_length = 0;
	uint8_t * rom_section, * ram_section;
	uint32_t rom_size = 0, ram_size = 0;

	rom_section = calloc(0x100000 * 32, 1);
	ram_section = calloc(0x100000 * 9, 1);

	fil = fopen(fn, "rb");

	if(!fil) {
		printf("Could not open USF!\n");
		return 0;
	}

	fread(buffer,4 ,1 ,fil);
	if(buffer[0] != 'P' && buffer[1] != 'S' && buffer[2] != 'F' && buffer[3] != 0x21) {
		printf("Invalid header in file!\n");
		fclose(fil);
		return 0;
	}

    fread(&reservedsize, 4, 1, fil);
    fread(&codesize, 4, 1, fil);
    fread(&crc, 4, 1, fil);

    fseek(fil, 0, SEEK_END);
    filesize = ftell(fil);

    reservestart = 0x10;
    tagstart = reservestart + reservedsize;
    tagsize = filesize - tagstart;

	if(tagsize) {
		fseek(fil, tagstart, SEEK_SET);
		fread(buffer, 5, 1, fil);

		if(buffer[0] != '[' && buffer[1] != 'T' && buffer[2] != 'A' && buffer[3] != 'G' && buffer[4] != ']') {
			printf("Errornous data in tag area! %ld\n", tagsize);
			fclose(fil);
			return 0;
		}

		buffer2 = malloc(50001);
		tagbuffer = malloc(tagsize);

    	fread(tagbuffer, tagsize, 1, fil);

		psftag_raw_getvar(tagbuffer,"_lib",buffer2,50000);

		if(strlen(buffer2)) {
			char path[512];
			int pathlength = 0;

			if(strrchr(fn, '/')) //linux
				pathlength = strrchr(fn, '/') - fn + 1;
			else if(strrchr(fn, '\\')) //windows
				pathlength = strrchr(fn, '\\') - fn + 1;
			else //no path
				pathlength = 0;//strlen(fn);

			strncpy(path, fn, pathlength);
			path[pathlength] = 0;
			strcat(path, buffer2);
			printf("Loading [%s]\n", path);
			//LoadUSF(path);
			usf_test_load_from_file(state, path);
		}

		psftag_raw_getvar(tagbuffer,"_enablecompare",buffer2,50000);
		if(strlen(buffer2))
			enablecompare = 1;
		else
			enablecompare = 0;

		psftag_raw_getvar(tagbuffer,"_enableFIFOfull",buffer2,50000);
		if(strlen(buffer2))
			enableFIFOfull = 1;
		else
			enableFIFOfull = 0;

		free(buffer2);
		buffer2 = NULL;
		free(tagbuffer);
		tagbuffer = NULL;
	}

/*	fseek(fil, reservestart, SEEK_SET);


	fread(&temp, 4, 1, fil);
	rom_size = 4;

	if(temp == 0x34365253) { //there is a rom section
		int len = 0, start = 0;
		uint32_t rom_start = ftell(fil);
		fread(&len, 4, 1, fil);
		rom_size += 4;

		while(len) {
			fread(&start, 4, 1, fil);
			rom_size += 4;

			while(len) {
				int page = start >> 16;
				int readLen = ( ((start + len) >> 16) > page) ? (((page + 1) << 16) - start) : len;
                	fread(rom_section + start, readLen, 1, fil);
                	rom_size += readLen;
				start += readLen;
				if((start + readLen) > rom_length)
					rom_length = start + readLen;
				len -= readLen;
			}
			fread(&len, 4, 1, fil);
			rom_size += 4;
		}

	}

	fread(&temp, 4, 1, fil);
	ram_size = 4;
	if(temp == 0x34365253) {
		int len = 0, start = 0;
		fread(&len, 4, 1, fil);
		ram_size += 4;

		while(len) {
			ram_size += 4;
			fread(&start, 4, 1, fil);
			ram_size += len;
			fread(ram_section + start, len, 1, fil);
			ram_size += 4;
			fread(&len, 4, 1, fil);
		}
	}


	uint32_t state_size;
	if(*(uint32_t*)(ram_section + 4) == 0x400000) {
		state_size = 0x40275c;

	} else if(*(uint32_t*)(ram_section + 4) == 0x800000)
		state_size = 0x80275c;

	uint32_t read_size = 0x10000000;*/
	fseek(fil, reservestart, SEEK_SET);
	fread(rom_section, 1, reservedsize, fil);

	rom_length = 32 * 0x100000;
	printf("[%08x]\n", rom_size);

    if(usf_upload_section(state, rom_section, reservedsize)) {
    		printf("Unable to load section for '%s'\n", fn);
    		return 3;
    	}
    /*if(!(usf_upload_section(state, rom_section + rom_size, reservedsize - rom_size))) {
    		printf("Unable to load ram section for '%s' len=%08x\n", fn, rom_length);
    		return 2;
    	}    */

	fclose(fil);

	return 1;
}

int main(int argc, char ** argv)
{
	const char * file_name = argv[1];//"sc2000.usf";
	int16_t buffer[65000];
	usf_state_t * _my_usf_state = NULL;
	const char * e = NULL;
	int count = 5000, counter = 0;
	int a = 0xdce7, b = 27;

     //printf("%08x\n", (((a * b + 0x4000)<<1)>>15));

	//exit(0);

    _my_usf_state = malloc(sizeof(usf_state_t) * 2);
	usf_clear(_my_usf_state);

	if(!usf_test_load_from_file(_my_usf_state, file_name)) {
		printf("Unable to load usf '%s'\n", file_name);
		return 1;
	}


	usf_set_compare(_my_usf_state, 1);
	usf_set_fifo_full(_my_usf_state, 0);
	if(argc > 2)
	    usf_set_hle_audio(_my_usf_state, 1);
	else
	    usf_set_hle_audio(_my_usf_state, 0);

	usf_set_hle_audio(_my_usf_state, 1);



	while(!(e = usf_render(_my_usf_state, buffer, count, &SampleRate))) {
		counter++;
		if(counter > 250)
			break;
        //printf("%d\n", counter);
		PlayBuffer(_my_usf_state, (char*)buffer, count * 4);
	}


	printf("Doneski [%s]\n", e);


	return 0;
}



